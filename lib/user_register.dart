import 'package:exampleapi/rest_api.dart';
import 'package:flutter/material.dart';

class User_register extends StatefulWidget {
  @override
  _User_registerState createState() => _User_registerState();
}

class _User_registerState extends State<User_register> {
  final _employeeNameController = TextEditingController();
  final _employeeAge = TextEditingController();
  final _password = TextEditingController();
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Employee'),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: <Widget>[
              TextField(
                controller: _employeeNameController,
                decoration: InputDecoration(hintText: 'Employee Name'),
              ),
              TextField(
                controller: _employeeAge,
                decoration: InputDecoration(hintText: 'Employee Age'),
                keyboardType: TextInputType.number,
              ),
              TextField(
                controller: _password,
                decoration: InputDecoration(hintText: 'Employee password'),
                keyboardType: TextInputType.number,
              ),
              RaisedButton(
                child: Text(
                  'SAVE',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                color: Colors.purple,
                onPressed: () {
                  final body = {
                    "otp": _password.text,
                    "email": _employeeAge.text,
                    "username": _employeeNameController.text,
                  };
                  ApiService.addEmployee(body).then((success) {
                    if (success) {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: Text('Employee has been added!!!'),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                                _employeeNameController.text = '';
                                _employeeAge.text = '';
                              },
                              child: Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    } else {
                      showDialog(
                        builder: (context) => AlertDialog(
                          title: Text('Error Adding Employee!!!'),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('OK'),
                            )
                          ],
                        ),
                        context: context,
                      );
                      return;
                    }
                  });
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
