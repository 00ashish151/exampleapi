import 'package:exampleapi/list_vew.dart';
import 'package:exampleapi/user_login.dart';
import 'package:exampleapi/user_register.dart';
import 'package:flutter/material.dart';

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter ListView - googleflutter.com'),
      ),
      body: ListView(
        children: <Widget>[
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => list_example()));
            },
            child: Container(
              height: 50,
              color: Colors.purple[600],
              child: const Center(
                  child: Text(
                'List View',
                style: TextStyle(fontSize: 18, color: Colors.white),
              )),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => User_register()));
            },
            child: Container(
              height: 50,
              color: Colors.purple[500],
              child: const Center(
                  child: Text(
                'User Register',
                style: TextStyle(fontSize: 18, color: Colors.white),
              )),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => Login_view()));
            },
            child: Container(
              height: 50,
              color: Colors.purple[400],
              child: const Center(
                  child: Text(
                'Login',
                style: TextStyle(fontSize: 18, color: Colors.white),
              )),
            ),
          ),
          Container(
            height: 50,
            color: Colors.purple[300],
            child: const Center(
                child: Text(
              'Item 4',
              style: TextStyle(fontSize: 18, color: Colors.white),
            )),
          ),
        ],
      ),
    );
  }
}
